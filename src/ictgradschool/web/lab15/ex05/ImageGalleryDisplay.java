package ictgradschool.web.lab15.ex05;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Servlet implementation class CGIParams
 */
public class ImageGalleryDisplay extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ImageGalleryDisplay() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		//response.getWriter().append("Served at: ").append(request.getContextPath());
		
		//displayBasic(request, response);
		
		displayHTML(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	private void displayHTML(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


			// http://stackoverflow.com/questions/1928675/servletrequest-getparametermap-returns-mapstring-string-and-servletreques
		
			// https://docs.oracle.com/javaee/6/api/javax/servlet/ServletRequest.html
			
		// "/" is relative to the context root (your web-app name)
		// don't add your web-app name to the path
		
		//RequestDispatcher view = request.getRequestDispatcher("/response.html"); // works, but /response.jsp causes 500 "jsp support not configured"
        //view.forward(request, response);

		PrintWriter out = response.getWriter();
		out.println("<html>\n<head><title>Server response</title>");
		out.println("</head>\n<body>");
		out.println("<h1>Server side response</h1>");
		out.println("<p>Thanks for your submission. The values sent to the server are as follows:</p>");
		out.println("<table>");

        File[] listOfFiles = new File(getServletContext().getRealPath("/Photos")).listFiles();

        String thumbnail = "_thumbnail.png";

        Map<String, File> map = new LinkedHashMap<>();

        for (File image: listOfFiles) {
            String fileName = image.getName();
            if (!fileName.endsWith(thumbnail)) map.put(fileName, image);
        }

        for (Entry<String, File> entry: map.entrySet()) {
            String src = entry.getKey();
            File image = entry.getValue();
            out.println("<tr>");

            out.println("<th>" + src.replace('_',' ').replace(".jpg","") + "<br>" + image.length() + " bytes</th>");
            out.println("<td><a href=\"../Photos/" + src + "\"><img src=\"../Photos/" + src.replace(".jpg", thumbnail) + "\"></a></td>");

            out.println("</tr>");
		}

		out.println("</table>");

		out.println("</body></html>");
	}
}
